#include "exampleplayer.h"
#include "board.h"
#include "common.h"
#include <iostream>
#include <fstream>

//this is a small change
//anotehr small change

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    curBoard = new Board();
    std::cerr << "blah" << std::endl;
    ourSide = side;
    if (ourSide == WHITE) otherSide = BLACK;
    else otherSide = WHITE;
}
//t
/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
    delete curBoard;
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 

    //team name is the bard
    /*time_t timer;
    int start = time(&timer);
    int curtim = time(&timer);
    while (curtim - start < 2)
    {
        curtim = time(&timer);
    }*/

    std::cerr << "doing move" << std::endl;
    curBoard->doMove(opponentsMove, otherSide);
    Move *ourMove = new Move(-1,-1);
    int ourMoveScore = 0;
    Board *testBoard = curBoard->copy();

    bool anyMoves = false;

    /*
    int numMoves = curBoard->numMoves(ourSide);
    Move* posMoves = curBoard->possibleMoves(numMoves, ourSide);
    if (numMoves==0) return NULL;
    else
    {
        for (int i=0; i<numMoves; i++)
        {
            Move *considerMove = &posMoves[i];
            delete testBoard;
            testBoard = curBoard->copy();
            testBoard->doMove(considerMove, ourSide);
            int considerMoveScore = testBoard->countBlack();
            if (considerMoveScore > ourMoveScore)
            {
                ourMove = considerMove;
                ourMoveScore = considerMoveScore;
            }
        }
    }*/

    for (int i=0; i<8; i++)
    {
        for (int j=0; j<8; j++)
        {
            Move *considerMove = new Move(i,j);
            if (curBoard->checkMove(considerMove, ourSide))
            {
                testBoard = curBoard->copy();
                testBoard->doMove(considerMove, ourSide);
                int testScore = testBoard->countBlack();
                if (testScore > ourMoveScore)
                {
                    anyMoves = true;
                    ourMove = considerMove;
                    ourMoveScore = testScore;
                }
            }
        }
    }

    if (anyMoves)
    {
        curBoard->doMove(ourMove, ourSide);
        return ourMove;
    }

    return NULL;
}
    
