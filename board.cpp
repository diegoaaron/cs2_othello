#include "board.h"
/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    int i;
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
    coefficients = (int*)calloc(64, sizeof(int));
    coefficients[0] = CORNER_GAIN;
    coefficients[7] = CORNER_GAIN;
    coefficients[63] = CORNER_GAIN;
    coefficients[56] = CORNER_GAIN;
    for(i = 2; i < 6; i++ )
    {
        coefficients[i] = SIDE_GAIN; 
    }
    for(i = 58; i < 62; i++ )
    {
        coefficients[i] = SIDE_GAIN; 
    }
    for ( i = 2; i < 6; ++i)
    {
        coefficients [8 * i] = SIDE_GAIN;
        coefficients [8 * i + 7] = SIDE_GAIN;
    }
    coefficients [1] = SIDE_LOSS;
    coefficients [6] = SIDE_LOSS;
    coefficients [8] = SIDE_LOSS;
    coefficients [9] = DIAG_LOSS;
    coefficients [14] = DIAG_LOSS;
    coefficients [15] = SIDE_LOSS;
    coefficients [48] = SIDE_LOSS;
    coefficients [49] = DIAG_LOSS;
    coefficients [54] = DIAG_LOSS;
    coefficients [55] = SIDE_LOSS;
    coefficients [57] = SIDE_LOSS;
    coefficients [62] = SIDE_LOSS;
}

/*
 * Destructor for the board.
 */
Board::~Board() {
    free(coefficients);
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board:: isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
int Board::numMoves(Side side) {
    int k = 0;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) k++;
        }
    }
    return k;
}

bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

Move* Board:: possibleMoves(int size, Side side){
    Move* list = (Move*)calloc(size, sizeof(Move));
    int iterator = 0;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side))
            {
                list[iterator] = Move(i, j);
                iterator++;
            }
        }
    }
    return list;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}
