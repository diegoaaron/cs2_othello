#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>

using namespace std;

class ExamplePlayer {



public:
    Board *curBoard;

    Side ourSide;
    Side otherSide;

    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
