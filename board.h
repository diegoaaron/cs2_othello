#ifndef __BOARD_H__
#define __BOARD_H__


#include <bitset>
#include "common.h"
#include <stdlib.h>
using namespace std;
#define CORNER_GAIN 10
#define SIDE_GAIN 5
#define SIDE_LOSS -4
#define DIAG_LOSS -8

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
    int* coefficients;   
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
        
    bool isDone();
    bool hasMoves(Side side);
    int numMoves(Side side);
    bool checkMove(Move *m, Side side);
    Move * possibleMoves(int size, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
};

#endif
